package com.wolftech.louvorsheknah.services;

import com.wolftech.louvorsheknah.dto.UsuarioDTO;
import com.wolftech.louvorsheknah.exception.ObjectAlreadyExistsException;
import com.wolftech.louvorsheknah.exception.ObjectNotFoundException;
import com.wolftech.louvorsheknah.mapper.UsuarioMapper;
import com.wolftech.louvorsheknah.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository repository;

    @Autowired
    private UsuarioMapper mapper;

    public UsuarioDTO salvar(UsuarioDTO dto){
        if (repository.findByCpf(dto.getCpf()).isPresent() && dto != null) {
            throw new ObjectAlreadyExistsException("Cpf já cadastrado: " + dto.getCpf());
        }
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    public UsuarioDTO editarUsuario(UsuarioDTO dto){
        if(!buscarUsuarioId(dto.getId()).isPresent()){
            throw new ObjectNotFoundException("Usuario não existe.");
        }
        return mapper.toDto(repository.save(mapper.toEntity(dto)));
    }

    public Optional<UsuarioDTO> buscarUsuarioId(Long id){
        if(!repository.findById(id).isPresent()){
            throw new ObjectNotFoundException("Usuario não existe.");
        }
        return Optional.of(mapper.toDto(repository.findById(id).get()));
    }

    public void deletarUsuario(Long id){
        if(!buscarUsuarioId(id).isPresent()){
            throw new ObjectNotFoundException("Usuario não existe.");
        }
            repository.deleteById(id);
    }

}
